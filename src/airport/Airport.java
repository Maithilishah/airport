/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

/**
 *
 * @author Admin
 */
public class Airport {
    private String location;
    private int airport_code;
    
    public Airport(String location, int airport_code) {
        this.location = location;
        this.airport_code = airport_code;
    }
    
    public String getLocation() {
        return this.location;
    }
    
     public int getAirport_code() {
        return this.airport_code;
    }
}
