/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

/**
 *
 * @author Admin
 */
public class Flight {
    
    private String number;
    private int date;
    
    public Flight(String number, int date) {
        this.number = number;
        this.date = date;
    }
    
    public String getNumber() {
        return this.number;
    }
    
     public int getdate() {
        return this.date;
    }
}
