/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airport;

/**
 *
 * @author Admin
 */
public class Airline {
    private String name;
    private int short_code;
    
    public Airline(String name, int short_code) {
        this.name = name;
        this.short_code = short_code;
    }
    
    public String getName() {
        return this.name;
    }
    
     public int getShort_code() {
        return this.short_code;
    }
}
